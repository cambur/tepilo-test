<?php


Route::prefix('leads')->group(function () {
    Route::get('/{email}', [
        'as' => 'leads.find',
        'uses' => LeadController::class . '@find',
    ]);

});
