<?php

namespace App\Services;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

/**
 * Class CloseService
 * @package Tepilo\Services
 */
class CloseService
{
    /**
     * @var string
     */
    protected $organizationId;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var string
     */
    protected $baseUrl;

    /**
     * @var string
     */
    protected $key;

    /**
     * CloseService constructor.
     */
    public function __construct()
    {
        $config = config('apiConnections.close');
        $this->validateConfig($config);
        $this->client = new Client();
        $this->baseUrl = $config['host'] . $config['version'];
        $this->key = $config['key'];
        $this->organizationId = $config['organization'];
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $options
     * @return mixed
     */
    public function syncRequest(string $method, string $url, array $options = [])
    {
        $options['auth'] = [
            $this->key,
            '',
        ];
        $result = $this->client->request($method, $url, $options);

        $response = \GuzzleHttp\json_decode($result->getBody(), true);

        return $response;
    }

    /**
     * @param string $email
     * @return ResponseInterface
     */
    public function findLead(string $email)
    {
        $options = [
            'limit' => '1',
            'organization_id' => $this->organizationId,
        ];

        $url = $this->baseUrl . '/lead' . '?query=' . $email;

        $result = $this->syncRequest('GET', $url, $options);

        return $result;
    }

    /**
     * @param string $email
     * @return ResponseInterface
     */
    public function createLead(array $lead)
    {
        $url = $this->baseUrl . '/lead';

        $result = $this->syncRequest('POST', $url, $lead);

        return $result;
    }

    /**
     * @param \ArrayAccess|array $config
     */
    protected function validateConfig($config)
    {
        if (empty($config['host'])) {
            throw new \InvalidArgumentException('config missing host');
        }
    }
}
