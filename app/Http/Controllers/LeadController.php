<?php

namespace App\Http\Controllers;

use App\Services\CloseioClient;
use App\Services\CloseService;

class LeadController extends Controller
{
    /**
     * @var CloseService
     */
    private $closeService;

    /**
     * @var CloseioClient
     */
    private $closeioClient;

    public function __construct(CloseioClient $closeioClient, CloseService $closeService)
    {
        $this->closeioClient = $closeioClient;
        $this->closeService = $closeService;
    }

    public function find(string $email)
    {
        $lead = $this->closeService->findLead($email)['data'][0];

        if (!$lead) {
            $response = $this->closeioClient->lead->create($this->getLead($email));

            return $response;
        }

        if (key_exists('custom.lcf_GEg9856gXoijAlF67G7ZXDtjSlycfNDxTaSyM6labnW', $lead)) {
            $lead['custom.lcf_GEg9856gXoijAlF67G7ZXDtjSlycfNDxTaSyM6labnW']++;
        } else {
            $lead['custom.lcf_GEg9856gXoijAlF67G7ZXDtjSlycfNDxTaSyM6labnW'] = "1";
        }

        $lead = $this->closeioClient->lead->update($lead['id'], $lead);

        return $lead;
    }

    private function getLead(string $email)
    {
        return [
            'name' => 'Francisco Perez',
            'url' => 'www.vice.com',
            'description' => '',
            'status_id' => 'stat_yE4J4QxxowV6IKNI931O7RrbtTn3iQtYwS9u52l4D2P',
            'contacts' => [
                [
                    'name' => 'Francisco Perez',
                    'emails' => [
                        [
                            'type' => 'office',
                            'email' => $email,
                        ],
                    ],
                    'phones' => [
                        [
                            'type' => 'office',
                            'phone' => '012345123123',
                        ],

                    ],
                ],
            ],
            "addresses" => [
                [
                    "label" => "business",
                    "address_1" => "747 Howard St",
                    "address_2" => "Room 3",
                    "city" => "San Francisco",
                    "state" => "CA",
                    "zipcode" => "94103",
                    "country" => "US",
                ],
            ],
        ];
    }
}
