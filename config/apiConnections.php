<?php

return [
    'close' => [
        'key' => env('CLOSE_API_KEY', null),
        'host' => env('CLOSE_API_URL', null),
        'version' => env('CLOSE_API_VERSION', null),
        'organization' => env('CLOSE_ORGANIZATION_ID', null),
    ],
];
