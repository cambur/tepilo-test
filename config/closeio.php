<?php

return [
    'base_url' => env('CLOSE_API_URL') . env('CLOSE_API_VERSION'),
    'api_key' => env('CLOSE_API_KEY'),
];
